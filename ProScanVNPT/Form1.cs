﻿using System.Diagnostics;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.StartPanel;

namespace ProScanVNPT
{
    public partial class Form1 : Form
    {
        HttpClient client = new HttpClient();
        private IList<Thread> _threads;
        Random random = new Random();
        public Form1()
        {
            InitializeComponent();
            datetime_picker_start.CustomFormat = "dd'/'MM'/'yyyy HH':'mm':'ss";
            datetime_picker_stop.CustomFormat = "dd'/'MM'/'yyyy HH':'mm':'ss";
            //client.DefaultRequestHeaders.Add("User-Agent", "PostmanRuntime/7.32.2");
            //client.DefaultRequestHeaders.Add("Accept", "*/*");
            //client.DefaultRequestHeaders.Add("Accept-Encoding", "gzip, deflate, br");
            //client.DefaultRequestHeaders.Add("Connection", "keep-alive");
            string fileName = @"token.txt";
            string token = "";
            try
            {
                token = File.ReadAllText(fileName);
            }
            catch (Exception ex)
            {

            }
            if (!client.DefaultRequestHeaders.Contains("Authorization"))
            {
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            }
            DecodeToken(token);
        }

        private async void button1_Click(object sender, EventArgs e)
        {

            string request = $@"{{
                                    ""username"": ""{tb_username.Text}"",
                                    ""password"": ""{tb_password.Text}""
                                    }}";
            var response = await callAPI(HttpMethod.Post, "https://api-onebss.vnpt.vn/quantri/user/xacthuc_tapdoan", request);
            string secretCode = extractValue(response, @"(?<=secretCode"":"").*?(?="")");
            string answer = Microsoft.VisualBasic.Interaction.InputBox("", "Nhập mã otp đăng nhập", "");
            request = $@"{{
                        ""grant_type"": ""password"",
                        ""client_id"": ""clientapp"",
                        ""client_secret"": ""password"",
                        ""secretCode"": ""{secretCode}"",
                        ""otp"": ""{answer}""
                        }}";
            response = await callAPI(HttpMethod.Post, "https://api-onebss.vnpt.vn/quantri/oauth/token", request);
            string token = extractValue(response, @"(?<=access_token"":"").*?(?="")");
            File.WriteAllText("token.txt", token);
            btn_start.Enabled = true;
        }

        async Task<string> callAPI(HttpMethod method, string url, String content)
        {
            HttpResponseMessage loginResponse = new HttpResponseMessage();
            loginResponse.Content = new StringContent("Init content response");
            try
            {
                var request = new HttpRequestMessage
                {
                    Method = method,
                    RequestUri = new Uri(url),
                    Content = new StringContent(content, Encoding.UTF8, "application/json")
                };
                loginResponse = await client.SendAsync(request);
            }
            catch (Exception ex)
            {

            }
            var loginResponseString = await loginResponse.Content.ReadAsStringAsync();
            string response = string.Format("Result API: {0}", loginResponseString);
            return loginResponseString;
        }

        private string extractValue(string data, string regex)
        {
            string output = "";
            // @"(?<={from}).*?(?={to})"
            // nếu {to} là 1 dấu "
            // thì phải thêm ""

            var res = Regex.Matches(data, regex, RegexOptions.Singleline);

            if (res != null && res.Count > 0)
            {
                output = res[0].ToString();
            }
            return output;
        }

        private void btn_start_Click(object sender, EventArgs e)
        {
            btn_start.Enabled = false;
            btn_stop.Enabled = true;
            tb_chua_ra_kho.Text = "";
            tb_ra_kho.Text = "";
            // reset, tạo mới list thread
            _threads = new List<Thread>();
            string[] phones = getListPhone();
            bool isUsingAlarm = datetime_picker_start.Checked;
            Thread x = new Thread(async () =>
            {
                if (isUsingAlarm)
                {
                    var time_from = DateTime.Now;
                    var time_to = datetime_picker_start.Value;
                    //SetLogTB(string.Format("Bắt số tại {0}", time_to.ToString("dd/MM/yyyy HH:mm:ss")));
                    AddText(tb_log, string.Format("Bắt số tại {0}", time_to.ToString("dd/MM/yyyy HH:mm:ss")));
                    Thread.Sleep(time_to.Subtract(time_from));
                }
                for (int i = 0; i < phones.Length; i++)
                {
                    int tempI = i;
                    Thread t = new Thread(() =>
                    {
                        Thread.Sleep(random.Next(Int32.Parse(tb_delay_from.Text), Int32.Parse(tb_delay_to.Text)));
                        ScanPhone(phones[tempI]);
                    });
                    t.Name = string.Format("MyThread-{0}", phones[tempI]);
                    t.IsBackground = true;
                    t.Start();
                    _threads.Add(t);

                }
            });
            x.Start();
            bool isUsingStopAlarm = datetime_picker_stop.Checked;
            Thread y = new Thread(() =>
            {
                if(isUsingStopAlarm)
                {
                    var time_from = DateTime.Now;
                    var time_to = datetime_picker_stop.Value;
                    Thread.Sleep(time_to.Subtract(time_from));
                    AddText(tb_log, string.Format("Hẹn giờ dừng tại {0}", time_to.ToString("dd/MM/yyyy HH:mm:ss")));
                    onEmptyPhone();
                }
            });
            y.Start();
        }

        void onEmptyPhone()
        {
            this.Invoke((MethodInvoker)delegate {
                btn_start.Enabled = true;
                btn_stop.Enabled = false;
            });
        }

        private string[] getListPhone()
        {
            string[] array = tb_phones.Text.Split(
                new string[] { Environment.NewLine },
                StringSplitOptions.RemoveEmptyEntries
            );
            return array;
        }

        private string FormatPhone(string phone)
        {
            phone = phone.Trim().Replace(".", "");
            if (phone.StartsWith("0")) phone = phone.Substring(1);
            if ((phone.StartsWith("84") && phone.Length < 10) || !phone.StartsWith("84"))
            {
                phone = "84" + phone;
            }
            return phone;
        }

        private async void ScanPhone(string phone)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            var _phone = phone.Split("|")[0];
            string request = $@"{{""so_tb"": ""{_phone}""}}";
            string tinh_trang = "";
            var response = await callAPI(HttpMethod.Get, "https://api-onebss.vnpt.vn/ccbs/executor/ts_tracuu_laytt_tb", request);
            if (response.Contains("đang nằm trong kho chọn số toàn quốc"))
            {
                // TRONG KHO
                AddTextNotDupplicate(tb_ra_kho, phone);
                RemovePhoneFromTextBox(tb_phones, phone);
                RemovePhoneFromTextBox(tb_chua_ra_kho, phone);
                tinh_trang = "TRONG KHO";
                PublishTinHieu(phone);
                SendLogBatSo(phone);
            }
            else if (response.Contains("ERR-001"))
            {
                // CHƯA RA KHO
                AddTextNotDupplicate(tb_chua_ra_kho, phone);
                RestartThread(phone);
                tinh_trang = "CHƯA RA KHO";
            }
            else
            {
                // CÒN LẠI
                tinh_trang = "ĐÃ XỊT";
            }
            sw.Stop();
            AddText(tb_log, string.Format("Time start {0}. Time process {1}s. SĐT {2}. Tình trạng {3}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), sw.ElapsedMilliseconds / 1000.0, _phone, tinh_trang));

        }

        // remove khỏi list thread và add thread mới vào
        public void RestartThread(string phone)
        {
            // Nếu đã tạm dừng / quá thời gian sống thì ko tạo thread mới nữa
            var now = DateTime.Now;
            var stop_time = datetime_picker_stop.Value;
            if(btn_start.Enabled)
            {
                return;
            }
            if (DateTime.Compare(stop_time, now) < 0 && datetime_picker_stop.Checked)
            {
                return;
            }



            {
                Thread t = new Thread(() =>
                {
                    Thread.Sleep(random.Next(Int32.Parse(tb_delay_from.Text), Int32.Parse(tb_delay_to.Text)));
                    ScanPhone(phone);
                });
                t.Start();
            }
        }

        public void AddTextNotDupplicate(TextBox textBox, string text)
        {
            Invoke(new Action(() =>
            {
                if (!textBox.Text.Contains(text))
                {
                    textBox.AppendText(text);
                    textBox.AppendText(Environment.NewLine);
                }
            }));
        }

        public void AddText(TextBox textBox, string text)
        {
            Invoke(new Action(() =>
            {
                textBox.AppendText(text);
                textBox.AppendText(Environment.NewLine);
            }));
        }

        public void RemovePhoneFromTextBox(TextBox textBox, string phone)
        {
            Invoke(new Action(() =>
            {
                // bỏ 2 số đầu đi để cover được 2 dạng: đầu 84, đầu 0
                textBox.Lines = textBox.Lines.Where(line => !line.Contains(phone.Substring(2))).ToArray();
            }));
        }

        private void btn_stop_Click(object sender, EventArgs e)
        {
            btn_start.Enabled = true;
            btn_stop.Enabled = false;
        }

        private void cb_test_mode_CheckedChanged(object sender, EventArgs e)
        {
            if (!btn_start.Enabled)
            {
                btn_start.Enabled = true;
            }
        }

        // Gửi tín hiệu đến server bắt số
        private void PublishTinHieu(string phone)
        {
            var _phone = phone.Split("|")[0];
            var _loai_thue_bao = phone.Split("|")[1];
            var list_server = phone.Split('|')[2].Split('&');
            for(int i = 0; i < list_server.Length; i++)
            {
                sendDangKy_TQ_TT(_phone, _loai_thue_bao, list_server[i].Split(":")[0], list_server[i].Split(":")[1]);
            }
            
        }

        private void sendDangKy_TQ_TT(string somay, string loai_thue_bao, string ip, string port)
        {
            string localip = "127.0.0.1";
            string message = "Dang ky" + "|" + somay + "|" + loai_thue_bao + "|" + "cuocCK" + "|" + "timeCK" + "|" + localip;
            sendMessageToServer(message, ip, Convert.ToInt32(port));
        }

        private async void SendLogBatSo(string phone)
        {
            var _phone = phone.Split("|")[0];
            var _loai_thue_bao = phone.Split("|")[1] == "TT" ? "TRATRUOC" : "TRASAU";
            string request = $@"{{
                                          ""type"": ""ONEBSS_PUBLISH_SUCCESS"",
                                          ""content"": ""{_phone}"",
                                          ""loai_thue_bao"": ""{_loai_thue_bao}"",
                                          ""username"": ""{tb_account.Text}""
                                        }}";
            var mxtResponse = await callAPI(HttpMethod.Post, "http://provnpt.com:8081/api/v1/maxacthuc", request);
        }

        public void sendMessageToServer(string thongdiep, string ip_server, int port)
        {
            try
            {
                IPAddress serverIP = IPAddress.Parse(ip_server);
                UdpClient sendClient = new UdpClient();
                byte[] message = System.Text.Encoding.ASCII.GetBytes(thongdiep);
                IPEndPoint endPoint = new IPEndPoint(serverIP, port);
                sendClient.Send(message, message.Length, endPoint);
                sendClient.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi khi gửi dữ liệu: " + ex.Message, "UDP Client", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void DecodeToken(string token)
        {
            var handler = new JwtSecurityTokenHandler();
            var jwtSecurityToken = handler.ReadJwtToken(token);
            tb_account.Text = jwtSecurityToken.Claims.First(claim => claim.Type == "ma_nhanvien_ccbs").Value;

            // thời hạn
            //string exp = jwtSecurityToken.Claims.First(claim => claim.Type == "exp").Value;
            //var exp_date = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddSeconds(long.Parse(exp));
            //tb_expire_time.Text = exp_date.ToString("dd/MM/yyyy HH:mm:ss");
        }

    }
}