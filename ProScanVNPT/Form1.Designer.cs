﻿namespace ProScanVNPT
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tb_username = new System.Windows.Forms.TextBox();
            this.tb_password = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.tb_phones = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tb_ra_kho = new System.Windows.Forms.TextBox();
            this.btn_start = new System.Windows.Forms.Button();
            this.tb_chua_ra_kho = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cb_test_mode = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tb_account = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.datetime_picker_stop = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.datetime_picker_start = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.tb_delay_to = new System.Windows.Forms.TextBox();
            this.tb_delay_from = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.tb_log = new System.Windows.Forms.TextBox();
            this.btn_stop = new System.Windows.Forms.Button();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tb_username
            // 
            this.tb_username.Location = new System.Drawing.Point(73, 20);
            this.tb_username.Name = "tb_username";
            this.tb_username.Size = new System.Drawing.Size(141, 23);
            this.tb_username.TabIndex = 0;
            // 
            // tb_password
            // 
            this.tb_password.Location = new System.Drawing.Point(73, 52);
            this.tb_password.Name = "tb_password";
            this.tb_password.Size = new System.Drawing.Size(141, 23);
            this.tb_password.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "Username";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 15);
            this.label2.TabIndex = 3;
            this.label2.Text = "Password";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(232, 20);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Login";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // tb_phones
            // 
            this.tb_phones.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.tb_phones.Location = new System.Drawing.Point(10, 131);
            this.tb_phones.Multiline = true;
            this.tb_phones.Name = "tb_phones";
            this.tb_phones.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tb_phones.Size = new System.Drawing.Size(914, 140);
            this.tb_phones.TabIndex = 5;
            this.tb_phones.Text = "84888888119|TS|127.0.0.1:1988&192.168.1.1:1980";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 113);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 15);
            this.label3.TabIndex = 6;
            this.label3.Text = "Số cần quét";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 439);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 15);
            this.label5.TabIndex = 10;
            this.label5.Text = "Đã ra kho";
            // 
            // tb_ra_kho
            // 
            this.tb_ra_kho.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.tb_ra_kho.Location = new System.Drawing.Point(12, 457);
            this.tb_ra_kho.Multiline = true;
            this.tb_ra_kho.Name = "tb_ra_kho";
            this.tb_ra_kho.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tb_ra_kho.Size = new System.Drawing.Size(912, 149);
            this.tb_ra_kho.TabIndex = 9;
            // 
            // btn_start
            // 
            this.btn_start.Enabled = false;
            this.btn_start.Location = new System.Drawing.Point(232, 55);
            this.btn_start.Name = "btn_start";
            this.btn_start.Size = new System.Drawing.Size(75, 22);
            this.btn_start.TabIndex = 11;
            this.btn_start.Text = "Start";
            this.btn_start.UseVisualStyleBackColor = true;
            this.btn_start.Click += new System.EventHandler(this.btn_start_Click);
            // 
            // tb_chua_ra_kho
            // 
            this.tb_chua_ra_kho.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.tb_chua_ra_kho.Location = new System.Drawing.Point(12, 292);
            this.tb_chua_ra_kho.Multiline = true;
            this.tb_chua_ra_kho.Name = "tb_chua_ra_kho";
            this.tb_chua_ra_kho.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tb_chua_ra_kho.Size = new System.Drawing.Size(912, 144);
            this.tb_chua_ra_kho.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 274);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 15);
            this.label6.TabIndex = 10;
            this.label6.Text = "Chưa ra kho";
            // 
            // cb_test_mode
            // 
            this.cb_test_mode.AutoSize = true;
            this.cb_test_mode.Location = new System.Drawing.Point(327, 24);
            this.cb_test_mode.Name = "cb_test_mode";
            this.cb_test_mode.Size = new System.Drawing.Size(103, 19);
            this.cb_test_mode.TabIndex = 12;
            this.cb_test_mode.Text = "Đã đăng nhập ";
            this.cb_test_mode.UseVisualStyleBackColor = true;
            this.cb_test_mode.CheckedChanged += new System.EventHandler(this.cb_test_mode_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tb_account);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.datetime_picker_stop);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.datetime_picker_start);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.tb_delay_to);
            this.groupBox2.Controls.Add(this.tb_delay_from);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Location = new System.Drawing.Point(489, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(435, 113);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Config app";
            // 
            // tb_account
            // 
            this.tb_account.Location = new System.Drawing.Point(67, 21);
            this.tb_account.Name = "tb_account";
            this.tb_account.ReadOnly = true;
            this.tb_account.Size = new System.Drawing.Size(114, 23);
            this.tb_account.TabIndex = 10;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 24);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 15);
            this.label8.TabIndex = 9;
            this.label8.Text = "Account";
            // 
            // datetime_picker_stop
            // 
            this.datetime_picker_stop.Checked = false;
            this.datetime_picker_stop.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.datetime_picker_stop.Location = new System.Drawing.Point(248, 83);
            this.datetime_picker_stop.Name = "datetime_picker_stop";
            this.datetime_picker_stop.ShowCheckBox = true;
            this.datetime_picker_stop.Size = new System.Drawing.Size(181, 23);
            this.datetime_picker_stop.TabIndex = 8;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(187, 89);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 15);
            this.label7.TabIndex = 7;
            this.label7.Text = "Giờ stop";
            // 
            // datetime_picker_start
            // 
            this.datetime_picker_start.Checked = false;
            this.datetime_picker_start.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.datetime_picker_start.Location = new System.Drawing.Point(248, 54);
            this.datetime_picker_start.Name = "datetime_picker_start";
            this.datetime_picker_start.ShowCheckBox = true;
            this.datetime_picker_start.Size = new System.Drawing.Size(181, 23);
            this.datetime_picker_start.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(187, 60);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 15);
            this.label4.TabIndex = 5;
            this.label4.Text = "Giờ start";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(337, 27);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(12, 15);
            this.label10.TabIndex = 4;
            this.label10.Text = "-";
            // 
            // tb_delay_to
            // 
            this.tb_delay_to.Location = new System.Drawing.Point(355, 24);
            this.tb_delay_to.Name = "tb_delay_to";
            this.tb_delay_to.Size = new System.Drawing.Size(74, 23);
            this.tb_delay_to.TabIndex = 3;
            this.tb_delay_to.Text = "3000";
            // 
            // tb_delay_from
            // 
            this.tb_delay_from.Location = new System.Drawing.Point(248, 24);
            this.tb_delay_from.Name = "tb_delay_from";
            this.tb_delay_from.Size = new System.Drawing.Size(83, 23);
            this.tb_delay_from.TabIndex = 2;
            this.tb_delay_from.Text = "1000";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(187, 27);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(36, 15);
            this.label11.TabIndex = 0;
            this.label11.Text = "Delay";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(12, 609);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(27, 15);
            this.label13.TabIndex = 19;
            this.label13.Text = "Log";
            // 
            // tb_log
            // 
            this.tb_log.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.tb_log.Location = new System.Drawing.Point(12, 627);
            this.tb_log.Multiline = true;
            this.tb_log.Name = "tb_log";
            this.tb_log.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tb_log.Size = new System.Drawing.Size(912, 135);
            this.tb_log.TabIndex = 18;
            // 
            // btn_stop
            // 
            this.btn_stop.Enabled = false;
            this.btn_stop.Location = new System.Drawing.Point(327, 54);
            this.btn_stop.Name = "btn_stop";
            this.btn_stop.Size = new System.Drawing.Size(75, 23);
            this.btn_stop.TabIndex = 20;
            this.btn_stop.Text = "Stop";
            this.btn_stop.UseVisualStyleBackColor = true;
            this.btn_stop.Click += new System.EventHandler(this.btn_stop_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(973, 774);
            this.Controls.Add(this.btn_stop);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.tb_log);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.cb_test_mode);
            this.Controls.Add(this.btn_start);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tb_chua_ra_kho);
            this.Controls.Add(this.tb_ra_kho);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tb_phones);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tb_password);
            this.Controls.Add(this.tb_username);
            this.Name = "Form1";
            this.Text = "ProScanVNPT v1.0.2";
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private TextBox tb_username;
        private TextBox tb_password;
        private Label label1;
        private Label label2;
        private Button button1;
        private TextBox tb_phones;
        private Label label3;
        private Label label5;
        private TextBox tb_ra_kho;
        private Button btn_start;
        private TextBox tb_chua_ra_kho;
        private Label label6;
        private CheckBox cb_test_mode;
        private GroupBox groupBox2;
        private Label label10;
        private TextBox tb_delay_to;
        private TextBox tb_delay_from;
        private Label label11;
        private Label label13;
        private TextBox tb_log;
        private Button btn_stop;
        private Button button2;
        private DateTimePicker datetime_picker_stop;
        private Label label7;
        private DateTimePicker datetime_picker_start;
        private Label label4;
        private TextBox tb_account;
        private Label label8;
    }
}